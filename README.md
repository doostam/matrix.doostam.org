# matrix.doostam.org

[Upstream URL](https://github.com/spantaleev/matrix-docker-ansible-deploy)

## Configuration

### Variables

* `IP_ADDRESS`:`159.203.187.58`
* `BASE_DOMAIN`:`doostam.org`
* `IHVARS`:`<BASE64 SECRET vars.yml>`
* `IHOSTS`:`<BASE64 SECRET hosts>`

### System

- Ubuntu 20.04

### DNS


| Type  | Name                  | Content                      | Proxy | TTL  |
| ------- | ----------------------- | ------------------------------ | ------- | ------ |
| A     | $BASE_DOMAIN          | $IP_ADDRESS                  | :x:   | Auto |
| CNAME | matrix                | $BASE_DOMAIN                 | :x:   | Auto |
| CNAME | chat                  | $BASE_DOMAIN                 | :x:   | Auto |
| CNAME | video                 | $BASE_DOMAIN                 | :x:   | Auto |
| SRV   | _matrix-identity._tcp | 10 0 443 matrix.$BASE_DOMAIN | :x:   | Auto |

### Playbook

1. `mkdir inventory/host_vars/matrix.$BASE_DOMAIN`
2. `echo $IHVARS > inventory/host_vars/matrix.$BASE_DOMAIN/vars.yml`)
3. `echo $IHOSTS > inventory/hosts`
